import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib.cm as cm
import Image
import pylab
from numpy.lib.financial import mirr

# load colored image as gray scale image
imagename = 'image100.png'
image = Image.open(imagename).convert("L")

# load the 2d structure of the gray scale image
gray2dimage = np.asarray(image)
# gray2dimage = mpimg.imread(imagename)

# 3-a plot all intensities in descending order
# reshape 2d array into 1d array
array1d = np.reshape(gray2dimage, -1);
# sort the 1d array in ascending order
array1d = np.sort(array1d)
# reverse the array
array1d = array1d[::-1]
# print(array1d)
# plt.plot(array1d)
# plt.show()

# 3-b show 20-bin histogram of intensities
# plt.hist(array1d, bins=20)
# plt.show()

# 3-c set Red when greater than t, black other wise
thres = 140
# create new array 100*100*3
rgb_image = np.zeros(((gray2dimage.shape[0], gray2dimage.shape[1], 3)))
# index on the new array if gray2dimage > thres set to red
rgb_image[gray2dimage > thres] = np.array([255, 0, 0])
# show image to see results
# plt.imshow(rgb_image)
# print(rgb_image)
# plt.show()

# 3-d show bottom left quadrant of the image
leftbottom = gray2dimage[gray2dimage.shape[0] / 2:, 0:gray2dimage.shape[1] / 2]
# plt.imshow(leftbottom,cmap=cm.Greys_r)
# print(leftbottom)
# plt.show()

# 3-e generate new image subtracted from the means
meanimg = gray2dimage - np.mean(array1d)
# pylab.imshow(gray2dimage, cmap = pylab.gray())
# print(meanimg)
# pylab.show()

# 3-f use numpy.random.rand to write a function that returns the roll of a six-sided die
dice = np.array([1, 2, 3, 4, 5, 6])
roll = np.random.rand(6)
# print(roll)
# print(np.max(roll))
rollout = dice[np.absolute(roll - np.max(roll)) < 0.0000001]
# print(rollout)

# 3-g use the argmax() function to set x to the maximum value that 
# occurs in A, and set r to the row it occurs in and c to the column it occurs in
ind = np.argmax(gray2dimage)
r = ind / gray2dimage.shape[1]
c = ind % gray2dimage.shape[1]
maxe = np.max(gray2dimage)
# print 'max element =' , maxe
# print 'index =',ind
# print 'row = ',r
# print 'column = ',c

# 3-h Let v be the vector: v = [1, 8, 8, 2, 1, 3, 9, 8]. Set a new variable x to be the number
# of 1 s in the vector v
v = [1, 8, 8, 2, 1, 3, 9, 8]
x = v.count(1)
# print(x)



# question 2
# 1- show image negative
negative = 255 - gray2dimage
# pylab.imshow(negative, cmap=pylab.gray())
# print(negative)
# pylab.show()

# 2- generate image mirror
mirror = np.fliplr(gray2dimage)
# pylab.imshow(mirror, cmap=pylab.gray())
# print(mirror)
# pylab.show()

# 3- reverse red and green color channel
colorimage = mpimg.imread('cimage.png')
reverse_rg = np.copy(colorimage)
tmp = np.copy(reverse_rg[:, :, 0])
reverse_rg[:, :, 0] = reverse_rg[:, :, 1]
reverse_rg[:, :, 1] = tmp
# print(reverse_rg)
# plt.imshow(reverse_rg)
# plt.show()

# 4- average between normal and mirror
average = (gray2dimage + mirror) / 2
# print(gray2dimage)
# print(mirror)
# pylab.imshow(average, cmap=pylab.gray())
# print(average)
# pylab.show()

# 5- add or subtract random
rand = np.random.randint(0, 255)
random = gray2dimage + rand
random[random > 255] = 255
# pylab.imshow(random, cmap=pylab.gray())
# print(random)
# pylab.show()

'''
#
fig = plt.figure()

# original image
a = fig.add_subplot(2, 4, 1)
plt.imshow(gray2dimage, cmap=cm.Greys_r)
a.set_title('Original Image')

# negative image
a = fig.add_subplot(2, 4, 2)
plt.imshow(negative, cmap=cm.Greys_r)
a.set_title('Negative')

# mirror
a = fig.add_subplot(2, 4, 3)
plt.imshow(mirror, cmap=cm.Greys_r)
a.set_title('Mirror')

# average
a = fig.add_subplot(2, 4, 4)
plt.imshow(average, cmap=cm.Greys_r)
a.set_title('Average')

# add random number
a = fig.add_subplot(2, 4, 5)
plt.imshow(random, cmap=cm.Greys_r)
a.set_title('Add random')

# original color
a = fig.add_subplot(2, 4, 6)
plt.imshow(colorimage)
a.set_title('Original Color')

# reverse channels
a = fig.add_subplot(2, 4, 7)
plt.imshow(reverse_rg)
a.set_title('Reverse channels')


plt.show()
'''
