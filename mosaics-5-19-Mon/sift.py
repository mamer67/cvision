import cv2
import numpy as np
import itertools

# reference = http://www.jayrambhia.com/blog/sift-keypoint-matching-using-python-opencv/

def findKeyPoints(img, template, distance=200):
    detector = cv2.FeatureDetector_create("SIFT")
    descriptor = cv2.DescriptorExtractor_create("SIFT")

    skp = detector.detect(img)
    skp, sd = descriptor.compute(img, skp)

    tkp = detector.detect(template)
    tkp, td = descriptor.compute(template, tkp)

    flann_params = dict(algorithm=1, trees=4)
    flann = cv2.flann_Index(sd, flann_params)
    idx, dist = flann.knnSearch(td, 1, params={})
    del flann

    dist = dist[:, 0] / 2500.0
    dist = dist.reshape(-1,).tolist()
    idx = idx.reshape(-1).tolist()
    indices = range(len(dist))
    indices.sort(key=lambda i: dist[i])
    dist = [dist[i] for i in indices]
    idx = [idx[i] for i in indices]
    
    skp_final = []
    for i, dis in itertools.izip(idx, dist):
        if dis < distance:
            skp_final.append(skp[i])
        else:
            break
    
    flann = cv2.flann_Index(td, flann_params)
    idx, dist = flann.knnSearch(sd, 1, params={})
    del flann

    dist = dist[:, 0] / 2500.0
    dist = dist.reshape(-1,).tolist()
    idx = idx.reshape(-1).tolist()
    indices = range(len(dist))
    indices.sort(key=lambda i: dist[i])
    dist = [dist[i] for i in indices]
    idx = [idx[i] for i in indices]
    tkp_final = []
    for i, dis in itertools.izip(idx, dist):
        if dis < distance:
            tkp_final.append(tkp[i])
        else:
            break
    
    # copy equal points
    minlength = min(len(skp_final), len(tkp_final))
    skp_final = skp_final[0:minlength]
    tkp_final = tkp_final[0:minlength]
    
    return skp_final, tkp_final

def drawKeyPoints(img, template, skp, tkp, num= -1):
    h1, w1 = img.shape[:2]
    h2, w2 = template.shape[:2]
    nWidth = w1 + w2
    nHeight = max(h1, h2)
    hdif = (h1 - h2) / 2
    newimg = np.zeros((nHeight, nWidth, 3), np.uint8)
    newimg[hdif:hdif + h2, :w2] = template
    newimg[:h1, w2:w1 + w2] = img

    maxlen = min(len(skp), len(tkp))
    if num < 0 or num > maxlen:
        num = maxlen
    for i in range(num):
        pt_a = (int(tkp[i].pt[0]), int(tkp[i].pt[1] + hdif))
        pt_b = (int(skp[i].pt[0] + w2), int(skp[i].pt[1]))
        cv2.line(newimg, pt_a, pt_b, (255, 0, 0))
    return newimg


def match():
    img = cv2.imread("smoha1.png")
    temp = cv2.imread("smoha2.png")
    
    dist = 3
    num = -1
    
    skp, tkp = findKeyPoints(img, temp, dist)
    
    print skp[0].pt
    print len(skp)
    print len(tkp)
    
    src_pts = np.zeros((len(skp), 2))
    dst_pts = np.zeros((len(skp), 2))
    
    for i in range(len(skp)):
        src_pts[i] = skp[i].pt
        dst_pts[i] = tkp[i].pt
    
    # Homography matrix M with RANSAC and threshold = 5.0
    M = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)[0]
    print M

    
    newimg = drawKeyPoints(img, temp, skp, tkp, num)
    cv2.imshow("image", newimg)
    cv2.waitKey(0)
    
match()
