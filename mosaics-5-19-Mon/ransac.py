from numpy import *
from random import choice

# DONT FORGET TO USE THIS WITH UR HOMOGRAPHY MATRIX at line 34

# START RANSAC
def ransac(points_list, iters=10 , error=10, good_model_num=5):
    
    model_error = 255
    model_H = None
    
    if (len(points_list) < 4):
        print "Error number of points less than 4"
        return None
        
    for i in range(iters):
        consensus_set = []
        pts0 = []
        pts1 = []
        points_list_temp = copy(points_list).tolist()
        
        # Randomly select 4 points
        for j in range(4):
            temp = choice(points_list_temp)
            x0, y0 = temp[0]
            x1, y1 = temp[1]
            pts0.append((x0, y0))
            pts1.append((x1, y1))
            consensus_set.append(temp)
            points_list_temp.remove(temp)
            
        # Calculate the homography matrix from the 4 points
        H = []
#         H = getHomographyMatrix((pts0, pts1))
        
        # Check if the other points fit this model
        for p in points_list_temp:
            x1, y1 = p[0]
            x2, y2 = p[1]

            A = array([x1, y1, 1]).reshape(3, 1)
            B = array([x2, y2, 1]).reshape(3, 1)
            
            Bp = dot(H, A)
            Bp /= Bp[2]
            out = B - Bp
            dist_err = hypot(out[0][0], out[1][0])
            if dist_err < error:
                consensus_set.append(p)                        
    
        # Check how well is our speculated model
        if len(consensus_set) >= good_model_num:
            dists = []
            for p in consensus_set:
                x0, y0 = p[0]
                x1, y1 = p[1]
                
                A = array([x0, y0, 1]).reshape(3, 1)
                B = array([x1, y1, 1]).reshape(3, 1)
                
                Bp = dot(H, A)
                Bp /= Bp[2]
                out = B - Bp
                dist_err = hypot(out[0][0], out[1][0])
                dists.append(dist_err)
            
            
            if (max(dists) < error) and (max(dists) < model_error):
                model_error = max(dists)
                model_H = H
                print "Finished at iteration ", i
                print "model_error", model_error
                break
        
    return model_H
# END RANSAC