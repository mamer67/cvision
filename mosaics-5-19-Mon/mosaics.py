from numpy import *
import cv2
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import itertools
from random import choice


# images/uttower1.jpg
# images/uttower2.jpg

# reference = http://www.jayrambhia.com/blog/sift-keypoint-matching-using-python-opencv/

# SIFT and RANSAC PART
def findKeyPoints(img, template, distance=200):
	detector = cv2.FeatureDetector_create("SIFT")
	descriptor = cv2.DescriptorExtractor_create("SIFT")
	
	skp = detector.detect(img)
	skp, sd = descriptor.compute(img, skp)
	
	tkp = detector.detect(template)
	tkp, td = descriptor.compute(template, tkp)
	
	flann_params = dict(algorithm=1, trees=4)
	flann = cv2.flann_Index(sd, flann_params)
	idx, dist = flann.knnSearch(td, 1, params={})
	del flann
	
	dist = dist[:, 0] / 2500.0
	dist = dist.reshape(-1,).tolist()
	idx = idx.reshape(-1).tolist()
	indices = range(len(dist))
	indices.sort(key=lambda i: dist[i])
	
	dist = [dist[i] for i in indices]
	idx = [idx[i] for i in indices]
	
	skp_final = []
	for i, dis in itertools.izip(idx, dist):
		if dis < distance:
			skp_final.append(skp[i])
		else:
			break
	
	flann = cv2.flann_Index(td, flann_params)
	idx, dist = flann.knnSearch(sd, 1, params={})
	del flann
	
	dist = dist[:, 0] / 2500.0
	dist = dist.reshape(-1,).tolist()
	idx = idx.reshape(-1).tolist()
	indices = range(len(dist))
	indices.sort(key=lambda i: dist[i])
	dist = [dist[i] for i in indices]
	idx = [idx[i] for i in indices]
	
	tkp_final = []
	for i, dis in itertools.izip(idx, dist):
		if dis < distance:
			tkp_final.append(tkp[i])
		else:
			break

	# copy equal points
	minlength = min(len(skp_final), len(tkp_final))
	skp_final = skp_final[0:minlength]
	tkp_final = tkp_final[0:minlength]
	
	return skp_final, tkp_final

def drawKeyPoints(img, template, skp, tkp, num= -1):
	h1, w1 = img.shape[:2]
	h2, w2 = template.shape[:2]
	nWidth = w1 + w2
	nHeight = max(h1, h2)
	hdif = (h1 - h2) / 2
	newimg = np.zeros((nHeight, nWidth, 3), np.uint8)
	newimg[hdif:hdif + h2, :w2] = template
	newimg[:h1, w2:w1 + w2] = img
	
	maxlen = min(len(skp), len(tkp))
	if num < 0 or num > maxlen:
		num = maxlen
	
	for i in range(num):
		pt_a = (int(tkp[i].pt[0]), int(tkp[i].pt[1] + hdif))
		pt_b = (int(skp[i].pt[0] + w2), int(skp[i].pt[1]))
		cv2.line(newimg, pt_a, pt_b, (255, 0, 0))
	return newimg
# END SIFT PART

# START RANSAC
def ransac(points_list, iters=10 , error=10, good_model_num=5):
	
	model_error = 255
	model_H = None
	
	if (len(points_list) < 4):
		print "Error number of points less than 4"
		return None
		
	for i in range(iters):
		consensus_set = []
		pts0 = []
		pts1 = []
		points_list_temp = copy(points_list).tolist()
		
		# Randomly select 4 points
		for j in range(4):
			temp = choice(points_list_temp)
			x0, y0 = temp[0]
			x1, y1 = temp[1]
			pts0.append((x0, y0))
			pts1.append((x1, y1))
			consensus_set.append(temp)
			points_list_temp.remove(temp)
			
		# Calculate the homography matrix from the 4 points
		H = getHomographyMatrix((pts0, pts1))
		
		# Check if the other points fit this model
		for p in points_list_temp:
			x1, y1 = p[0]
			x2, y2 = p[1]

			A = array([x1, y1, 1]).reshape(3, 1)
			B = array([x2, y2, 1]).reshape(3, 1)
			
			Bp = dot(H, A)
			Bp /= Bp[2]
			out = B - Bp
			dist_err = hypot(out[0][0], out[1][0])
			if dist_err < error:
				consensus_set.append(p)						
	
		# Check how well is our speculated model
		if len(consensus_set) >= good_model_num:
			dists = []
			for p in consensus_set:
				x0, y0 = p[0]
				x1, y1 = p[1]
				
				A = array([x0, y0, 1]).reshape(3, 1)
				B = array([x1, y1, 1]).reshape(3, 1)
				
				Bp = dot(H, A)
				Bp /= Bp[2]
				out = B - Bp
				dist_err = hypot(out[0][0], out[1][0])
				dists.append(dist_err)
			
			
			if (max(dists) < error) and (max(dists) < model_error):
				model_error = max(dists)
				model_H = H
				print "Finished at iteration ", i
				print "model_error", model_error
				break
		
	return model_H
# END RANSAC

# Manually identifies corresponding points from two views
def getCorrespondence(imageA, imageB):
	# Display images, select matching points
	fig = plt.figure()
	figA = fig.add_subplot(1, 2, 1)
	figB = fig.add_subplot(1, 2, 2)
	# Display the image
	figA.imshow(imageA)
	figB.imshow(imageB)
	plt.axis('image')
	pts = plt.ginput(n=8, timeout=0)
	pts = np.reshape(pts, (2, 4, 2))
	plt.close('all')
	
	return pts
			
def getHomographyMatrix(pts):
	a = ones((8, 8));
	b = ones ((8, 1))
	
	# pts[0] = points in image1 (without dashes)
	# pts[1] = points in image2 (with dashes)
	j = 0
	for i in range(0, 8, 2):
		a[i][0] = pts[0][j][0]  # x
		a[i][1] = pts[0][j][1]  # y
		a[i][2] = 1
		a[i][3] = 0
		a[i][4] = 0
		a[i][5] = 0
		a[i][6] = -pts[0][j][0] * pts[1][j][0]  # -x x'
		a[i][7] = -pts[0][j][1] * pts[1][j][0]  # -x'y

		a[i + 1][0] = 0
		a[i + 1][1] = 0
		a[i + 1][2] = 0
		a[i + 1][3] = -pts[0][j][0]  # -x
		a[i + 1][4] = -pts[0][j][1]  # -y
		a[i + 1][5] = -1
		a[i + 1][6] = pts[0][j][0] * pts[1][j][1]
		a[i + 1][7] = pts[0][j][1] * pts[1][j][1]
		
		b[i] = pts[1][j][0]  # x'
		b[i + 1] = -pts[1][j][1]  # -y'
		i += 2
		j += 1
	
	H = ones((3, 3))
	
	try:
		x = linalg.solve(a, b)	
		H[0][0] = x[0]
		H[0][1] = x[1]
		H[0][2] = x[2]
		H[1][0] = x[3]
		H[1][1] = x[4]
		H[1][2] = x[5]
		H[2][0] = x[6]
		H[2][1] = x[7]
		H[2][2] = 1
	except np.linalg.linalg.LinAlgError as err:
		if 'Singular matrix' in err.message:
			return H
		else:
			raise
	
	return H

def biLinearInterpolation(imgA, r, c):
	rows = imgA.shape[0]
	columns = imgA.shape[1]
# 	print r,ce
	output = zeros((1, 3))
	row = (int)(r)
	col = (int)(c)
	
	b = r - row; a = c - col
# 	print a,b
	output += imgA[row][col] * ((1 - a) * (1 - b))
	if(row + 1 < rows):
		output += imgA[row + 1][col] * ((a) * (1 - b))
	if(rows + 1 < rows and col + 1 < columns):
		output += imgA[row + 1][col + 1] * (a * b)
	if(col + 1 < columns):
		output += imgA[row][col + 1] * ((1 - a) * (b))
	output = output.astype(int)
# 	print "before ", imgA[row][col], " After ", output, r,c
	return output

def imageMosaics(imgA, imgB, H):
	topLeftCorner = dot(H, [0, 0, 1])
	topLeftCorner /= topLeftCorner[2]  # convert it to [x,y,1]

	topRightCorner = dot (H, [imgA.shape[1], 0, 1])
	topRightCorner /= topRightCorner[2]
	
	buttonLeftCorner = dot (H, [0, imgA.shape[0], 1])
	buttonLeftCorner /= buttonLeftCorner[2]
	
	buttonRightCorner = dot (H, [imgA.shape[1], imgA.shape[0], 1])
	buttonRightCorner /= buttonRightCorner[2]
	
	leftX = min(topLeftCorner[0], buttonLeftCorner[0])
	rightX = max(topRightCorner[0], buttonRightCorner[0])
	
	topY = min(topLeftCorner[1], topRightCorner[1])
	buttonY = max(buttonLeftCorner[1], buttonRightCorner[1])
	
	width = imgB.shape[1]
	height = imgB.shape[0]
	
	outputImgWidth = width
	outputImgHeight = height
	
	startX = 0
	startY = 0
	
	if(leftX < 0):
		startX = abs(leftX)
		outputImgWidth += abs(leftX)
	
	if(rightX > width):
		outputImgWidth += rightX - width
	
	if(topY < 0):
		startY = abs(topY)
		outputImgHeight += abs(topY)
	
	if(buttonY > height):
		outputImgHeight += buttonY - height;
		
	print "corners"
	print topLeftCorner, topRightCorner, buttonRightCorner, buttonLeftCorner
	
	print "image a dimension"
	print imgA.shape[1], imgA.shape[0]
	print "image b dimension"
	print width, height
	print "output image dimension"
	print outputImgWidth, outputImgHeight
	
	# initialize the outputImage will (-1,-1,-1)
	outputImage = -1 * ones((outputImgHeight, outputImgWidth, 3))
	print outputImage.shape
	
	
	'''		
	# copy imgB into the outputImage
	for i in range(0, imgB.shape[0]):
		for j in range(0, imgB.shape[1]):
			outputImage[i + startY][j + startX] = imgB[i][j]
			
	# inverse warp
	for i in range(0, outputImage.shape[0]):
		for j in range(0, outputImage.shape[1]):
			if(outputImage[i][j][0] == -1):
				x = j
				y = i 
				original = dot(linalg.inv(H) , [x - startX, y - startY, 1])
				original /= original[2]

				ip = original[1]
				jp = original[0]
				
				if(ip > 0 and ip < imgA.shape[0] and jp > 0 and jp < imgA.shape[1]):
# 					outputImage[i][j] = biLinearInterpolation(imgA, ip, jp)
					outputImage[i][j] = imgA[ip][jp]  # open this to disable interpolation
	'''
	
	for i in range(0, outputImage.shape[0]):
		for j in range(0, outputImage.shape[1]):
				ip = i - startY
				jp = j - startX
				
				if(ip >= 0 and ip < imgB.shape[0] and jp >= 0 and jp < imgB.shape[1]):
					outputImage[i][j] = imgB[ip][jp]
				
				x = j
				y = i 
				original = dot(linalg.inv(H) , [x - startX, y - startY, 1])
				original /= original[2]

				ip = original[1]
				jp = original[0]
				
				
				if(ip >= 0 and ip < imgA.shape[0] and jp >= 0 and jp < imgA.shape[1]):
					if (outputImage[i][j][0] == -1) or (outputImage.shape[0] <= max(imgA.shape[0], imgB.shape[0]) 
												and outputImage.shape[1] <= max(imgA.shape[1], imgB.shape[1]) 
												and sum(abs(outputImage[i][j] - imgA[ip][jp])) > 5):
# 						outputImage[i][j] = biLinearInterpolation(imgA, ip, jp)
						outputImage[i][j] = imgA[ip][jp]
	
# 	for i in range(0, imgA.shape[0]):
# 		for j in range(0, imgA.shape[1]):
# 			x = j
# 			y = i 
# 			original = dot(H , [x, y, 1])
# 			original /= original[2]
# 			ip = (int)(original[1])
# 			jp = (int)(original[0])
# 			
# 			outputImage[startY + ip][startX + jp] = imgA[i][j]

	# we should continue from here
	# 1) put imgB as it is in a specific position in the output image
	# 2) loop on the holes in the output image and calculate H inverse * (X, Y,1)
	# where X, Y is the coordinate of the hole
	# the output of this multiplication will give you a pixel in imgA
	# we should make some interpolation between 4 pixels
	# then put the interpolated colors in the position (X,Y) in the output image
	
	
	print "done"
	return outputImage

# RUN METHODS

# run with prestored homography matrix
def run_default(imageA, imageB):
	# Image Mosaics
	H = ones((3, 3))
	
	H[0] = [  7.01187033e-01, 5.83161592e-02, 4.51417173e+02]
	H[1] = [ -1.64134612e-01, 9.12112705e-01, 8.09468530e+01]
	H[2] = [ -2.75920163e-04, -6.26414149e-06, 1.00000000e+00]
	
	print H
	
	print "h"
	outputImage = imageMosaics(imageA, imageB, H)
	cv2.imwrite("output.jpg", outputImage)
	
	return H
	
	
	
# run with mouse as input
def run_mouse(imageA, imageB):
	pts = getCorrespondence(imageA, imageB)
	H = getHomographyMatrix(pts)
	print H
	
	outputImage = imageMosaics(imageA, imageB, H)
	cv2.imwrite("output.jpg", outputImage)
	
	return H

# run with sift 
def run_siftv1(imageA, imageB, dist, num, ransacerror):
	# use the sift and Ransac
	skp, tkp = findKeyPoints(imageA, imageB, dist)
	
	numpoints = len(skp)
	print "feature point", numpoints
	
	src_pts = np.zeros((len(skp), 2))
	dst_pts = np.zeros((len(skp), 2))
	
	for i in range(len(skp)):
		src_pts[i] = skp[i].pt
		dst_pts[i] = tkp[i].pt
	
	# Homography matrix M with RANSAC and threshold = 5.0	
	H = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, ransacerror)[0]
	print H
	
	outputImage = imageMosaics(imageA, imageB, H)
	cv2.imwrite("output.jpg", outputImage)
	
	newimg = drawKeyPoints(imageA, imageB, skp, tkp, num)
	
	# show image feature
	cv2.imshow("image", newimg)
	cv2.waitKey(0)
	
	return H

def run_siftv2(imageA, imageB, dist, num, maxiter, ransacerror, thresh):
	# use the sift and Ransac
	skp, tkp = findKeyPoints(imageA, imageB, dist)
	
	numpoints = len(skp)
	print "feature point", numpoints
	
	src_pts = np.zeros((len(skp), 2))
	dst_pts = np.zeros((len(skp), 2))
	
	for i in range(len(skp)):
		src_pts[i] = skp[i].pt
		dst_pts[i] = tkp[i].pt
	
	arr = list(zip(src_pts, dst_pts))
	
	M = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, ransacerror)[0]
	print "Homography using cv2\n", M
	
	# iteration, error, good_model_num
	H = ransac(arr, maxiter, ransacerror, thresh * numpoints)
	print "My ransac Homography\n", H
	
	outputImage = imageMosaics(imageA, imageB, H)
	cv2.imwrite("output.jpg", outputImage)
	
	newimg = drawKeyPoints(imageA, imageB, skp, tkp, num)
	
	# show image feature
	cv2.imshow("image", newimg)
	cv2.waitKey(0)
	
	return H

def open_H_plot(imageA, imageB, H):
	# Display images, select matching points
	fig = plt.figure()
	
	figA = fig.add_subplot(1, 2, 1)
	figB = fig.add_subplot(1, 2, 2)
	
	
	# Display the image
	figA.imshow(imageA)
	figB.imshow(imageB)
	plt.axis('image')	
	
	while(1):
		pts = plt.ginput(n=1, timeout=0)
		
		if len(pts) == 0:
			break
		
		# get point in second image
		A = [pts[0][0], pts[0][1], 1]
		Bp = dot(H, A)
		Bp /= Bp[2]
		
		print A
		print Bp
		
		transFigure = fig.transFigure.inverted()

		coord1 = transFigure.transform(figA.transData.transform([A[0], A[1]]))
		coord2 = transFigure.transform(figB.transData.transform([Bp[0], Bp[1]]))
		
		line = matplotlib.lines.Line2D((coord1[0], coord2[0]), (coord1[1], coord2[1]),
                               transform=fig.transFigure)
				
		fig.lines = line,
# 		fig.lines.append(line)

# 		figA.plot(A[0], A[1], 'p', markersize=5) 		
# 		figB.plot(Bp[0], Bp[1], 'p', markersize=5)
		

# main method
# Read image file names
# fileA = raw_input("Please insert the file name for the first image: ")
# fileB = raw_input("Please insert the file name for the second image: ")
imageA = cv2.imread("smoha1.png")
imageB = cv2.imread("smoha2.png")

# run default H 
# H = run_default(imageA, imageB)

# run mouse
# H = run_mouse(imageA, imageB)

# run sift + cv2.Homography, min dist in sift, num of lines, min sse error
# H = run_siftv1(imageA, imageB, 4.0, -1, 4.0)

# smoha 4.0, -1, 1000000, 5.0, 0.3
# uttower 4.0, -1, 1000000, 1.0, 0.05
# mountain 4.0, -1, 1000000, 1.0, 0.3
# run sift + my ransac, min dist in sift, num of lines, max iteration, min sse error, min inlier points
H = run_siftv2(imageA, imageB, 4.0, -1, 1000000, 5.0, 0.3)

print "Test Homgoraphy now"
open_H_plot(imageA, imageB, H)
