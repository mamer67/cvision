import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib.cm as cm
import Image
import pylab
from numpy.lib.financial import mirr


nx, ny = (3, 2)

x = np.linspace(0, 1, nx)
y = np.linspace(0, 1, ny)

print x
print y

xv, yv = np.meshgrid(x, y)

print xv
print yv