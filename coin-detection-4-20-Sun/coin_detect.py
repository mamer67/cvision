'''
Created on Apr 17, 2014
 
@author: Ahmed A'amer
'''

import cv2
import numpy
import matplotlib.pyplot as plt
from numpy.lib.scimath import sqrt
from numpy import *
from copy import copy
 
def toGrayscale(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    return gray
 
def medianFilter(img):
    median = cv2.medianBlur(img, 7)
    return median
 
def cannyEdgeDetection(img):
    lo = 250;
    hi = 280;
    return cv2.Canny(img, lo, hi)
 
def topNValues(img, N, radius):
    array = -1 * numpy.ones((N, 3));
    imgtmp = numpy.copy(img)
    
    # get indices of max element
    index = unravel_index(imgtmp.argmax(), imgtmp.shape)
    # set max element
    array[0][0] = index[0]
    array[0][1] = index[1]
    array[0][2] = imgtmp[index[0]][index[1]]
    imgtmp[index[0]][index[1]] = 0
    
    threshold = 0.69 * array[0][2]
    
    print array[0][0], array[0][1], array[0][2]
    
    d = 0
    i = 1
    # get the max
    while (i < N):        
        # set flag to 1
        insert = 1
        # get indices of max element
        index = unravel_index(imgtmp.argmax(), imgtmp.shape)
        
        if(imgtmp[index[0]][index[1]] >= threshold):
            # check proximity with previous coins
            for j in range(i):
                d = sqrt(pow(array[j][0] - index[0], 2) + pow(array[j][1] - index[1], 2))
                if(d < 2 * radius):
                    insert = 0
                    break
        
            if (insert == 1):
                array[i][0] = index[0]
                array[i][1] = index[1]
                array[i][2] = imgtmp[index[0]][index[1]]
                print array[i][0], array[i][1], array[i][2], d, radius
                i = i + 1
                
            # ignore the highest 
            imgtmp[index[0]][index[1]] = 0
        else:
            break
            
    return array

def nonMaxSupression(img, rows, columns):
    # 8 directions
    x = [1, 1, 1, -1, -1, -1, 0, 0]
    y = [0, -1, 1, 0, -1, 1, 1, -1]
    
    output = copy(img)
    for i in range(rows):
        for j in range(columns):
            ok = 1
            
            for k in range(8):
                if i + x[k] >= 0 and i + x[k] < rows and j + y[k] >= 0 and j + y[k] < columns:
                    if img[i][j] < img[i + x[k]][j + y[k]]:
                        ok = 0
                        break
                    
            if(ok == 0):
                output[i][j] = 0
            else:
                output[i][j] = img[i][j]
    return output       
 
def getVotes(img, rows, columns, radius, bins):
    rows = (int)(rows / bins)
    columns = (int)(columns / bins)
    H = numpy.zeros((rows, columns))
   
    # get nonzero from numpy
    indx = numpy.nonzero(img)
    
    # loop on the edges of the input image
    for k in range(indx[0].shape[0]):
        y = indx[0][k]
        x = indx[1][k]
        for a in range(x - radius, x + radius):
            b1 = y + sqrt(pow(radius, 2) - pow((x - a), 2))
            b2 = y - sqrt(pow(radius, 2) - pow((x - a), 2))
            
            a = (int) (a / bins)
            b1 = (int) (b1 / bins)
            b2 = (int) (b2 / bins)
            
            # vote now
            if(a > 0 and a < columns):
                if(b1 > 0 and b1 < rows):
                    H[(int)(b1)][(int)(a)] += 1
                if(b2 > 0 and b2 < rows):
                    H[(int)(b2)][(int)(a)] += 1
    return H

def drawBorders(H, oradius, radius, inputOutput, color, oldrow, bins):
#     the index of the maximum value
    nonH = nonMaxSupression(H, H.shape[0], H.shape[1])
    top = topNValues(nonH, 20, radius)
    
    total = 0
    
    # draw circle
    thickness = 4    
    for i in range(top.shape[0]):
        b = (int) (top[i][0])
        a = (int) (top[i][1])
        
        if(a < 0 or b < 0):
            break
        
        if(bins > 1):
            b = (int)(b * bins) + 1
            a = (int)(a * bins) + 1
        else:
            b = (int)(b * oldrow / H.shape[0])
            a = (int)(a * oldrow / H.shape[0])
        
        total = total + 1
        print a, b
        cv2.circle(inputOutput, (a, b), oradius, color, thickness)
        
    return total

def houghTransform(imgresized, original, oldrow):
    shape = imgresized.shape
    rows = shape[0]
    columns = shape[1]
   
    bins = 1
    
    # 1 pound
    oradius = 135
    radius = (int)(135.0 * rows / oldrow)
    H = getVotes(imgresized, rows, columns, radius, bins)
    cv2.imwrite('a-R-135.jpg', H)

    # mark the borders in the image
    color = (0, 0, 255)
    num = drawBorders(H, oradius, radius, original, color, oldrow, bins)
    
    total = num
    
    # half pound
    oradius = 120
    radius = (int)(120.0 * rows / oldrow)
    H = getVotes(imgresized, rows, columns, radius, bins)
    cv2.imwrite('a-R-120.jpg', H)
    color = (0, 255, 0)
    num = drawBorders(H, oradius, radius, original, color, oldrow, bins)
     
    total = total + num * 0.5
     
    # quarter pound
    oradius = 107
    radius = (int)(107.0 * rows / oldrow)
    H = getVotes(imgresized, rows, columns, radius, bins)
    cv2.imwrite('a-R-107.jpg', H)
    color = (255, 0, 0)
    num = drawBorders(H, oradius, radius, original, color, oldrow, bins)
     
    total = total + num * 0.25
    
    return total
    
 
# main method
imageName = "coins_1.jpg"

coloredImage = cv2.imread(imageName)
copyImage = copy(coloredImage)

# resize image
oldrow = coloredImage.shape[0]
imgresized = cv2.resize(coloredImage, (0, 0), fx=0.09, fy=0.09) 
# imgresized = copyImage

grayscale = toGrayscale(imgresized)
smoothedImage = medianFilter(grayscale)
cannyImage = cannyEdgeDetection(smoothedImage)


total = houghTransform(cannyImage, copyImage, oldrow)

print "total is = ", total
 
# plt.hist(cannyImage, bins=20)
# plt.show()
 
# save images
# cv2.imwrite(folderName + "original.jpg", coloredImage)
cv2.imwrite("a-grayscale.jpg", grayscale)
cv2.imwrite("a-smoothedImage.jpg", smoothedImage)
cv2.imwrite("a-canny.jpg", cannyImage)
cv2.imwrite('a-hough.jpg', copyImage)
 
 
# negative in drawBorder
# edges not full 
 
# show images
# cv2.imshow('Smoothed Image', smoothedImage)
# cv2.imshow('Canny Image', cannyImage)
# cv2.waitKey(0)
