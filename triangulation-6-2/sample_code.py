import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from mpl_toolkits.mplot3d.axes3d import Axes3D

# Draws corresponding points
# I1: first image
# I2: second image
# matches: matcing points
def drawCorrespondence(I1, I2, matches):
	img = np.array (np.hstack((I1, I2)))
	img[:] = np.hstack((I1, I2))
	# Plot corresponding points
	radius = 2
	thickness = 2
	for m in matches:
		# draw the keypoints
		pt1 = (int(m[0]), int(m[1]))
		pt2 = (int(m[2] + I1.shape[1]), int(m[3]))
		lineColor = (255, 0, 0)
		ptColor = (0, 255, 0)
		cv2.circle(img, pt1, radius, ptColor, thickness)
		cv2.line(img, pt1, pt2, lineColor, thickness)
		cv2.circle(img, pt2, radius, ptColor, thickness)
	cv2.imshow("Matches", img)


def draw(I1, I2, xs, ys, zs, P1, P2, point1, point2):
	# Plot corresponding points
	radius = 4
	radius2 = 2
	thickness = 2
	
	for i in range(0, len(point1)):
		P = (xs[i], ys[i], zs[i], 1)
		p1d = np.dot(P1, P)
		p2d = np.dot(P2, P)
		
		p1d /= p1d[2]
		p2d /= p2d[2]
		
# 		p1 = p1d[0:1]
# 		p2 = p2d[0:1]
		
		ptColor = (0, 0, 255)
		pt1 = (int(point1[i][0]), int(point1[i][1]))
		cv2.circle(I1, pt1, radius, ptColor, thickness)
		ptColor = (0, 255, 0)
		pt1 = (int(p1d[0]), int(p1d[1]))
		cv2.circle(I1, pt1, radius2, ptColor, thickness)
		
		ptColor = (0, 0, 255)
		pt1 = (int(point2[i][0]), int(point2[i][1]))
		cv2.circle(I2, pt1, radius, ptColor, thickness)
		ptColor = (0, 255, 0)
		pt1 = (int(p2d[0]), int(p2d[1]))
		cv2.circle(I2, pt1, radius2, ptColor, thickness)
		
		
	cv2.imwrite("r1.png", I1)
	cv2.imwrite("r2.png", I2)

def open_H_plot(imageA, imageB, F):
	# Display images, select matching points
	fig = plt.figure()
	
	figA = fig.add_subplot(1, 2, 1)
	figB = fig.add_subplot(1, 2, 2)
	
	# Display the image
	figA.imshow(imageA)
	figB.imshow(imageB)
	plt.axis('image')	
	
	while(1):
		pts = plt.ginput(n=1, timeout=0)
		
		if len(pts) == 0:
			break
		
		# get point in second image
		A = [pts[0][0], pts[0][1], 1]
		print A
		print F
		Bp = np.dot(F, A)
		Bp /= Bp[2]
		
		pt1 = (0, int(-1 / Bp[1]))
		pt2 = (int(-1 / Bp[0]), 0)
		
		
		transFigure = fig.transFigure.inverted()

		coord1 = transFigure.transform(figB.transData.transform([pt1[0], pt1[1]]))
		coord2 = transFigure.transform(figB.transData.transform([pt2[0], pt2[1]]))
		
		line = matplotlib.lines.Line2D((coord1[0], coord2[0]), (coord1[1], coord2[1]),
                               transform=fig.transFigure, lw=2.5)
				
# 		fig.lines = line,
		fig.lines.append(line)

# this functions assumes w=1 when solving the linear system of equations
def solveSys1(pts1, pts2, P1, P2):
	xs = np.zeros([len(pts1)])
	ys = np.zeros([len(pts1)])
	zs = np.zeros([len(pts1)])    
	for i in range(0, len(pts1)):
		a = np.zeros([4, 4])
		a[0] = np.array(pts1[i][0] * P1[2] - P1[0])
		a[1] = np.array(pts1[i][1] * P1[2] - P1[1])
		a[2] = np.array(pts2[i][0] * P2[2] - P2[0])
		a[3] = np.array(pts2[i][1] * P2[2] - P2[1])
		A = np.zeros([4, 3])
		B = np.zeros([4, 1])
		for j in range(0, 4):
			A[j] = a[j][0:3]
			B[j] = -1 * a[j][3]
		X = np.linalg.lstsq(A, B)
		xs[i] = X[3][0]
		ys[i] = X[3][1]
		zs[i] = X[3][2]
	
	return (xs, ys, zs)

# use decomposition for solving
def solveSys2(pts1, pts2, P1, P2):
	xs = np.zeros([len(pts1)])
	ys = np.zeros([len(pts1)])
	zs = np.zeros([len(pts1)])
	ws = np.zeros([len(pts1)])     
	for i in range(0, len(pts1)):
		A = np.zeros([4, 4])
		A[0] = pts1[i][0] * P1[2].T - P1[0].T
		A[1] = pts1[i][1] * P1[2].T - P1[1].T
		A[2] = pts2[i][0] * P2[2].T - P2[0].T
		A[3] = pts2[i][1] * P2[2].T - P2[1].T
		zeros = np.zeros((3))
		C, X = cv2.solve(A, zeros, flags=cv2.DECOMP_SVD)
# 		print V
# 		X=V[:,3]
# 		print X
# 		ws[i] = X[3]
# 		X /= X[3]
		
		xs[i] = X[0] / X[3]
		ys[i] = X[1] / X[3]
		zs[i] = X[2] / X[3]
		
	return (xs, ys, zs, ws)

def report(xs, ys, zs, ws, pts1, pts2, P1, P2):
	
	for i in range(0, len(pts1)):
		P = (xs[i], ys[i], zs[i], ws[i])
		p1d = np.dot(P1, P)
		p2d = np.dot(P2, P)
		
		p1d /= p1d[2]
		p2d /= p2d[2]
		
		p1 = (p1d[0], p1d[1])
		p2 = (p2d[0], p2d[1])
		
		print "P1 diff ", np.absolute(p1[0] - pts1[i][0])+np.absolute(p1[1] - pts1[i][1]), " P2 diff ", np.absolute(p2[0] - pts2[i][0])+np.absolute(p2[1] - pts2[i][1])
		
def run():
	prefix = "checker"
	
	# Load images and match files for the first example
	I1 = cv2.imread("assgn5_data/" + prefix + "1.jpg");
	I2 = cv2.imread("assgn5_data/" + prefix + "2.jpg");
	# Load matching points
	matches = np.loadtxt("assgn5_data/" + prefix + "_matches.txt"); 
	# Load projection matrix for both cameras
	P1 = np.loadtxt("assgn5_data/" + prefix + "1_camera.txt")
	P2 = np.loadtxt("assgn5_data/" + prefix + "2_camera.txt")
	# This is a N x 4 file where the first two numbers of each row
	# are coordinates of corners in the first image and the last two
	# are coordinates of corresponding corners in the second image: 
	# matches(i,3:4) is a corresponding point in the second image

	# display two images side-by-side with matches
	# this code is to help you visualize the matches, you don't need
	# to use it to produce the results for the assignment
	# drawCorrespondence(I1, I2, matches)

	point1 = np.zeros((matches.shape[0], 2))
	point2 = np.zeros((matches.shape[0], 2))
	
	for i in range(0, matches.shape[0]):
		m = matches[i]
		# draw the keypoints
		point1[i] = (m[0], m[1])
		point2[i] = (m[2], m[3])
		
	# Calculating the Fundamental Matrix goes here
	fundamental_matrix = cv2.findFundamentalMat(point1, point2, cv2.FM_RANSAC, 3, 0.99);
	
	print "fundamental matrix is \n", fundamental_matrix[0]
	
	# Visualize Epipolar lines here
# 	open_H_plot(I1, I2, fundamental_matrix[0])
	
	# Triangulation code goes here...
	# xs, ys, zs = solveSys1(point1, point2, P1, P2)

	# use decomposition for solving
	xs, ys, zs, ws = solveSys2(point1, point2, P1, P2)
	
	# get camera centers
	C1 = np.linalg.svd(P1)[2][:, 3]
	C2 = np.linalg.svd(P2)[2][:, 3]
	
	C1 = C1 / C1[3]
	C2 = C2 / C2[3]
	
	cx = (C1[0], C2[0])
	cy = (C1[1], C2[1])
	cz = (C1[2], C2[2])
	
	fig = plt.figure()
	ax = Axes3D(fig)
	ax.scatter(xs, ys, zs, "*")
	ax.plot(cx, cy, cz, "+")
	plt.show()
	
	# print difference
	# ws = np.ones([len(point1)])
	report(xs, ys, zs, ws, point1, point2, P1, P2)
	
	draw(I1, I2, xs, ys, zs, P1, P2, point1, point2)
	
	cv2.waitKey(0)

if __name__ == "__main__":
	run()
