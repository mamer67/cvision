import cv2
import numpy as np
import matplotlib.pyplot as plt

file = open('input.txt', 'r')
test_cases = int(file.readline())

for x in range(0, test_cases):    
    # set image name
    image_name = file.readline()
    image_name = image_name.split("\n")[0]
    image_out = image_name.split(".")
    
    # load image
    rgb_image = cv2.imread(image_name)
    
    # convert to gray image
    gray_image = cv2.cvtColor(rgb_image, cv2.COLOR_BGR2GRAY)
    
    # save image
    cv2.imwrite(image_out[0] + "Gray." + image_out[1], gray_image)
      
    # create new smooth matrix
    smooth_median = cv2.medianBlur(gray_image, 7)
    
    # save image
    cv2.imwrite(image_out[0] + "Median." + image_out[1], smooth_median)
    
    # create laplace on image
    laplacian_image = cv2.Laplacian(smooth_median, cv2.CV_8U, ksize=5)
    
    # save image
    # cv2.imwrite("2-laplace.png", laplacian_image)
    
    # create laplace on image
    th1, threshold_image = cv2.threshold(laplacian_image, 125, 255, cv2.THRESH_BINARY_INV)
    
    # save image
    cv2.imwrite(image_out[0] + "Binary." + image_out[1], threshold_image)
    
    # create new image
    bilateral_image = np.copy(rgb_image)
    
    for x in range(0, 7):
        bilateral_image = cv2.bilateralFilter(bilateral_image, 9, 9, 7)
        
    # save image
    cv2.imwrite(image_out[0] + "Bilateral." + image_out[1], bilateral_image)
    
    # create cartoon
    cartoon = np.copy(bilateral_image)
    cartoon[:, :, 0] = bilateral_image[:, :, 0] & threshold_image
    cartoon[:, :, 1] = bilateral_image[:, :, 1] & threshold_image
    cartoon[:, :, 2] = bilateral_image[:, :, 2] & threshold_image
    
    # save image
    cv2.imwrite(image_out[0] + "Final." + image_out[1], cartoon)
    
       
    '''
    # show image
    cv2.imshow('color_image', rgb_image)
    cv2.imshow('gray_image', gray_image)
    cv2.imshow('smooth_median', smooth_median)
    cv2.imshow('laplacian_image', laplacian_image)
    cv2.imshow('threshold', threshold_image)
    cv2.imshow('bilateral_image', bilateral_image)
    cv2.imshow('cartoon_image', cartoon)
    
    # Waits forever for user to press any key
    cv2.waitKey(0) 
    
    # Closes displayed windows
    cv2.destroyAllWindows()  
    '''


