import cv

# set image name
image_name = "oldman.png"

# load image
rgb_image = cv.LoadImage(image_name)

# create new matrix
gray_image = cv.CreateMat(rgb_image.height, rgb_image.width, cv.CV_8UC1)

# convert to gray image
cv.CvtColor(rgb_image, gray_image, cv.CV_BGR2GRAY)

# save image
cv.SaveImage("0-grayscale.png", gray_image)
  
# create new matrix
smooth_median = cv.CreateMat(rgb_image.height, rgb_image.width, cv.CV_8UC1)

# create new smooth matrix
cv.Smooth(gray_image, smooth_median, cv.CV_MEDIAN, 7)

# save image
cv.SaveImage("1-median.png", smooth_median)

# create new image
laplacian_image = cv.CreateMat(rgb_image.height, rgb_image.width, cv.CV_8UC1)

# create laplace on image
cv.Laplace(smooth_median, laplacian_image, 5)

# save image
cv.SaveImage("2-laplace.png", laplacian_image)


# create new image
threshold_image = cv.CreateMat(rgb_image.height, rgb_image.width, cv.CV_8UC1)

# create laplace on image
cv.Threshold(laplacian_image, threshold_image, 125, 255, cv.CV_THRESH_BINARY_INV)

# save image
cv.SaveImage("3-threshold.png", threshold_image)


# create new image
bilateral_image = cv.CreateMat(rgb_image.height, rgb_image.width, cv.CV_8UC3)

# create laplace on image cv.Smooth(tmp, smallImg, cv.CV_BILATERAL, size, 0, sigmaColor, sigmaSpace)
cv.Smooth(rgb_image, bilateral_image, cv.CV_BILATERAL, 9, 0, 9, 7);


# save image
cv.SaveImage("4-bilateral.png", bilateral_image)

# for x in range(0, 3):
    
# show image
cv.NamedWindow("Old Man Gray", cv.CV_WINDOW_AUTOSIZE)
cv.ShowImage("Old Man Gray", bilateral_image)
# cv.ShowImage("Old Man Median Smooth", smooth_median)

# wait for key
cv.WaitKey(0)
# cv.DestroyWindow("Old Man")
